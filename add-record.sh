#!/bin/bash

# Test an IP address for validity:
# Usage:
#      valid_ip IP_ADDRESS
#      if [[ $? -eq 0 ]]; then echo good; else echo bad; fi
#   OR
#      if valid_ip IP_ADDRESS; then echo good; else echo bad; fi
#
# source https://www.linuxjournal.com/content/validating-ip-address-bash-script
function valid_ip()
{
    local  ip=$1
    local  stat=1

    if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
            && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        stat=$?
    fi
    return $stat
}

function is_in_list()
{
	local ip=${1}
	local occurrences=$(echo ${ip} | cat deny-ip-list.txt - | sort -h | uniq -d | wc -l)
	[[ $((${occurrences})) -ne 0 ]]
	return $?
}

if ! [ ${#} -eq 1 -o ${#} -eq 2 ]
then
	echo "This script add attacke'sIP address to deny-ip-list.txt"
	echo "Usage ${0} <attacker's IPv4 address> [Mikrotik FW]"
	echo "	attacker's address - IPv4 address"
	echo "	Mikrotik FW - user@address used by ssh client"
	echo "		- if defined attacker's address is also added to 'synology-dsm-attackers' list on Mikrotik device"
	exit 1
fi

FW_ADDRESS=${2}
IP=${1}; 
if ! valid_ip ${IP}
then
	echo "'${IP}' is not valid IPv4 address"
	exit 1
fi

if is_in_list ${IP}
then
	echo "IP is already in list"
	exit 1
fi

echo ${IP} >> deny-ip-list.txt && \
       	sort -h deny-ip-list.txt > deny-ip-list.tmp && \
       	uniq -u deny-ip-list.tmp > deny-ip-list.txt && \
       	rm deny-ip-list.tmp && \
	git add deny-ip-list.txt && \
       	git commit -m "\"added IP ${IP}\""
if [ ${?} -eq 0 -a ${#} -eq 2 ]
then
	ssh ${FW_ADDRESS} "/ip/firewall/address-list/add list=synology-dsm-attackers address=${IP}"
fi
